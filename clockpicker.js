/**
 * @file
 * Javascript for Clockpicker.
 */

(function ($) {
  Drupal.behaviors.clockpicker = {
    attach: function(context, settings) {
      jQuery.each(Drupal.settings.clockpicker_classes, function(indexInArray, clockpickerClass) {
        $('.' + clockpickerClass).clockpicker({
          donetext: 'Done',
          autoclose: 1
        });
      });
    }
  };
})(jQuery);
