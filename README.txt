Clockpicker
===========

This module integrates the Clockpicker library to allow it to be used on date
fields.

## Setup

Download the Clockpicker library from http://weareoutman.github.io/clockpicker/
and install it as a Drupal library, e.g. in sites/all/libraries.

Ensure you have jQuery 1.7 or higher, for instance by using jQuery Update
module.

## Configuration

Date fields must be set to use the 'date popup' widget. Edit the field's
settings and enabled the 'Use the clockpicker for times' option.
